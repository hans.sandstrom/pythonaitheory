# Jupyter
# With scipy notebook
# AUTHOR                Hans S.
# VERSION               1.0

FROM jupyter/scipy-notebook

RUN mkdir -p files/workbooks
RUN mkdir -p files/data

RUN pip3 -q install pip --upgrade
RUN pip3 -q install names
RUN pip3 -q install faker

COPY ./workbooks files/workbooks
COPY ./misc files/misc

RUN jupyter trust files/workbooks/*.ipynb

WORKDIR files/workbooks
