# MLTheory
Teori för maskinlära

## Prerequisites
To run it please install:
* Docker
* Docker compose

Read more:

https://www.docker.com/

https://docs.docker.com/compose/


## To run it
In order to run the jupyter for this class please do this:

+ Open up your terminal of choice.

+ Build
`docker-compose build`

+ Run
`docker-compose up`

+ Find the link in the output from the above command. It should have this format.
http://127.0.0.1:8888/?token=THETOKEN

+ Le voilà!

Read more:

https://jupyter-notebook.readthedocs.io/en/stable/security.html
